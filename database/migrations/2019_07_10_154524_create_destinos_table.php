<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destino', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ciudad_id');
            $table->foreign('ciudad_id')->references('id')->on('ciudad');
            $table->string('de_titpequeno',100);
            $table->string('de_titprincipal',100);
            $table->string('de_descripcioncorta',250);
            $table->text('de_descripcionlarga');
            $table->text('de_img1');
            $table->text('de_img2');
            $table->text('de_img3');
            $table->char('de_estado',1);
            $table->dateTime('de_registerDate');
            $table->dateTime('de_registerUpdate');
            $table->char('de_userUpdate',2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destino');
    }
}
