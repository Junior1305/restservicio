<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentaCarrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renta_carro', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ciudad_id');
            $table->foreign('ciudad_id')->references('id')->on('ciudad');
            $table->string('rc_titulo',100);
            $table->string('rc_descripcioncorta',250);
            $table->text('rc_descripcionlarga');
            $table->text('rc_img1');
            $table->text('rc_img2');
            $table->text('rc_img3');
            $table->char('rc_estado',1);
            $table->dateTime('rc_registerDate');
            $table->dateTime('rc_registerUpdate');
            $table->char('rc_userUpdate',2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renta_carro');
    }
}
