<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ciudad_id');
            $table->foreign('ciudad_id')->references('id')->on('ciudad');
            $table->string('ho_titpequeno',100);
            $table->string('ho_titprincipal',100);
            $table->string('ho_descripcioncorta',250);
            $table->text('ho_descripcionlarga');
            $table->text('ho_img1');
            $table->text('ho_img2');
            $table->text('ho_img3');
            $table->char('ho_estrella',1);
            $table->char('ho_estado',1);
            $table->dateTime('ho_registerDate');
            $table->dateTime('ho_registerUpdate');
            $table->char('ho_userUpdate',2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel');
    }
}
