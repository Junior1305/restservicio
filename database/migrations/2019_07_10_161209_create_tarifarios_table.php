<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTarifariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarifario', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('destino_id');
            $table->foreign('destino_id')->references('id')->on('destino');
            $table->unsignedBigInteger('proveedor_id');
            $table->decimal('ta_precio',5,2);
            $table->char('ta_estado',1);
            $table->dateTime('ta_registerDate');
            $table->dateTime('ta_registerUpdate');
            $table->char('ta_userUpdate',2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarifario');
    }
}
