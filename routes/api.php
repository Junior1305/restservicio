<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::group(['middleware' => 'cors'],function() {
	Route::post('getPais','PaisController@show');
	Route::post('createPais','PaisController@store');
	Route::get('listPais','PaisController@list');
	Route::delete('deletePais','PaisController@destroy');
	Route::put('updatePais','PaisController@update');

	Route::post('getCiudad','CiudadController@show');
	Route::post('createCiudad','CiudadController@store');
	Route::get('listCiudad','CiudadController@list');
	Route::delete('deleteCiudad','CiudadController@destroy');
	Route::put('updateCiudad','CiudadController@update');
});
