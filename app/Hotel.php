<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
	public $table = "hotel";
	public $timestamps = false;
	protected $fillable = [
		'ciudad_id','ho_titpequeno','ho_titprincipal','ho_descripcioncorta','ho_descripcionlarga','ho_img1','ho_img2','ho_img3','ho_estrella','ho_estado','ho_registerDate','ho_registerUpdate','ho_userUpdate'
	];
}
