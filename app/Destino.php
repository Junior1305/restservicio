<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destino extends Model
{
	public $table = "destino";
	public $timestamps = false;
	protected $fillable = [
		'ciudad_id','de_titpequeno','de_titprincipal','de_descripcioncorta','de_descripcionlarga','de_img1','de_img2','de_img3','de_estado','de_registerDate','de_registerUpdate','de_userUpdate'
	];
}
