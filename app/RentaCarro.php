<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RentaCarro extends Model
{
	public $table = "renta_carro";
	public $timestamps = false;
	protected $fillable = [
		'ciudad_id','rc_titulo','rc_descripcioncorta','rc_descripcionlarga','rc_img1','rc_img2','rc_img3','rc_estado','rc_registerDate','rc_registerUpdate','rc_userUpdate'
	];
}
