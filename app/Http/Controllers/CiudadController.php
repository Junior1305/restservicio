<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Ciudad;
use Validator; 

class CiudadController extends BaseController
{

	public function __construct(){
		$this->middleware('cors');
	}

	public function index()
	{
		$ciudades = Ciudad::all();
		return $this->sendResponse($ciudades->toArray(), 'Ciudades enviados exitosamente.');
	}


	public function list()
	{

		$ciudades = Ciudad::where('ci_estado','1')->get();
		return $this->sendResponse($ciudades->toArray(), 'Ciudades enviados exitosamente.');
	}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$input['ci_registerDate'] = date('Y-m-d H:i:s');
    	$input['ci_registerUpdate'] = date('Y-m-d H:i:s');
    	
    	if($input['ci_nombre'] == '' || $input['pais_id'] == ''){
    		return $this->sendError('','Ciudad no registrado.');       
    	}

    	$ciudad = Ciudad::create($input);


    	return $this->sendResponse($ciudad->toArray(), 'Ciudad creada exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);

    	$ciudad = Ciudad::find($input['id']);

    	if (is_null($ciudad)) {
    		return $this->sendError(null,'Ciudad no encontrado.');
    	}


    	return $this->sendResponse($ciudad->toArray(), 'Ciudad enviado exitosamente.');
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$ciudad = Ciudad::find($input['id']);
    	$input['ci_registerUpdate'] = date('Y-m-d H:i:s');
    	if (is_null($ciudad)) {
    		return $this->sendError($ciudad->toArray(),'Ciudad no encontrado.');
    	}

    	$ciudad->ci_nombre = $input['ci_nombre'];
    	$ciudad->ci_registerUpdate = $input['ci_registerUpdate'];
    	$ciudad->ci_userUpdate = $input['ci_userUpdate'];

    	$ciudad->save();


    	return $this->sendResponse($ciudad->toArray(), 'Ciudad actualizado exitosamente.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$ciudad = Ciudad::find($input['id']);
    	$ciudad->delete();
    	return $this->sendResponse($ciudad->toArray(), 'Ciudad eliminada exitosamente.');
    }
}



