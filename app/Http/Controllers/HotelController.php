<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Hotel;
use Validator; 

class HotelController extends BaseController
{

	public function __construct(){
		$this->middleware('cors');
	}

	public function index()
	{
		$hoteles = Hotel::all();
		return $this->sendResponse($hoteles->toArray(), 'Hoteles enviados exitosamente.');
	}


	public function list()
	{

		$hoteles = Hotel::where('ho_estado','1')->get();
		return $this->sendResponse($hoteles->toArray(), 'Hoteles enviados exitosamente.');
	}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$input['ho_registerDate'] = date('Y-m-d H:i:s');
    	$input['ho_registerUpdate'] = date('Y-m-d H:i:s');
    	
    	if($input['ho_titprincipal'] == '' || $input['ciudad_id'] == null ){
    		return $this->sendError('','Hotel no registrado.');       
    	}

    	$hotel = Hotel::create($input);


    	return $this->sendResponse($hotel->toArray(), 'Hotel creada exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);

    	$hotel = Hotel::find($input['id']);

    	if (is_null($hotel)) {
    		return $this->sendError(null,'Hotel no encontrado.');
    	}


    	return $this->sendResponse($hotel->toArray(), 'Hotel enviado exitosamente.');
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$hotel = Hotel::find($input['id']);
    	$input['ho_registerUpdate'] = date('Y-m-d H:i:s');
    	if (is_null($hotel)) {
    		return $this->sendError($hotel->toArray(),'Hotel no encontrado.');
    	}

    	$hotel->ciudad_id = $input['ciudad_id'];
    	$hotel->ho_titpequeno = $input['ho_titpequeno'];
    	$hotel->ho_titprincipal = $input['ho_titprincipal'];
    	$hotel->ho_descripcioncorta = $input['ho_descripcioncorta'];
    	$hotel->ho_descripcionlarga = $input['ho_descripcionlarga'];
    	$hotel->ho_img1 = $input['ho_img1'];
    	$hotel->ho_img2 = $input['ho_img2'];
    	$hotel->ho_img3 = $input['ho_img3'];
    	$hotel->ho_estrella = $input['ho_estrella'];
    	$hotel->de_registerUpdate = $input['de_registerUpdate'];
    	$hotel->de_userUpdate = $input['de_userUpdate'];

    	$hotel->save();


    	return $this->sendResponse($hotel->toArray(), 'Hotel actualizado exitosamente.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$hotel = Hotel::find($input['id']);
    	$hotel->delete();
    	return $this->sendResponse($hotel->toArray(), 'Hotel eliminada exitosamente.');
    }
}



