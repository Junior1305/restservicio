<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Tarifario;
use Validator; 

class TarifarioController extends BaseController
{

	public function __construct(){
		$this->middleware('cors');
	}

	public function index()
	{
		$tarifario = Tarifario::all();
		return $this->sendResponse($tarifario->toArray(), 'Tarifario enviados exitosamente.');
	}


	public function list()
	{

		$tarifario = Tarifario::where('ta_estado','1')->get();
		return $this->sendResponse($tarifario->toArray(), 'Tarifario enviados exitosamente.');
	}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$input['ta_registerDate'] = date('Y-m-d H:i:s');
    	$input['ta_registerUpdate'] = date('Y-m-d H:i:s');
    	
    	if($input['proveedor_id'] == '' || $input['ta_precio'] == null || $input['destino_id'] ){
    		return $this->sendError('','Tarifario no registrado.');       
    	}

    	$tarifario = Tarifario::create($input);


    	return $this->sendResponse($tarifario->toArray(), 'Tarifario creada exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);

    	$tarifario = Tarifario::find($input['id']);

    	if (is_null($tarifario)) {
    		return $this->sendError(null,'Tarifario no encontrado.');
    	}


    	return $this->sendResponse($tarifario->toArray(), 'Tarifario enviado exitosamente.');
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$tarifario = Tarifario::find($input['id']);
    	$input['ta_registerUpdate'] = date('Y-m-d H:i:s');
    	if (is_null($tarifario)) {
    		return $this->sendError($tarifario->toArray(),'Tarifario no encontrado.');
    	}

    	$tarifario->proveedor_id = $input['proveedor_id'];
    	$tarifario->destino_id = $input['destino_id'];
    	$tarifario->ta_precio = $input['ta_precio'];
    	$tarifario->ta_registerUpdate = $input['ta_registerUpdate'];
    	$tarifario->ta_userUpdate = $input['ta_userUpdate'];

    	$tarifario->save();


    	return $this->sendResponse($tarifario->toArray(), 'Tarifario actualizado exitosamente.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$tarifario = Tarifario::find($input['id']);
    	$tarifario->delete();
    	return $this->sendResponse($tarifario->toArray(), 'Tarifario eliminada exitosamente.');
    }
}



