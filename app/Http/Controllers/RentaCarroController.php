<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\API\BaseController as BaseController;
use App\RentaCarro;
use Validator; 

class RentaCarroController extends BaseController
{

	public function __construct(){
		$this->middleware('cors');
	}

	public function index()
	{
		$rentacarros = RentaCarro::all();
		return $this->sendResponse($rentacarros->toArray(), 'RentaCarros enviados exitosamente.');
	}


	public function list()
	{

		$rentacarros = RentaCarro::where('rc_estado','1')->get();
		return $this->sendResponse($rentacarros->toArray(), 'RentaCarros enviados exitosamente.');
	}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$input['rc_registerDate'] = date('Y-m-d H:i:s');
    	$input['rc_registerUpdate'] = date('Y-m-d H:i:s');
    	
    	if($input['rc_titulo'] == '' || $input['ciudad_id'] == null ){
    		return $this->sendError('','RentaCarro no registrado.');       
    	}

    	$rentacarro = RentaCarro::create($input);


    	return $this->sendResponse($rentacarro->toArray(), 'RentaCarro creada exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);

    	$rentacarro = RentaCarro::find($input['id']);

    	if (is_null($rentacarro)) {
    		return $this->sendError(null,'RentaCarro no encontrado.');
    	}


    	return $this->sendResponse($rentacarro->toArray(), 'RentaCarro enviado exitosamente.');
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$rentacarro = RentaCarro::find($input['id']);
    	$input['rc_registerUpdate'] = date('Y-m-d H:i:s');
    	if (is_null($rentacarro)) {
    		return $this->sendError($rentacarro->toArray(),'RentaCarro no encontrado.');
    	}

    	$rentacarro->ciudad_id = $input['ciudad_id'];
    	$rentacarro->rc_titulo = $input['rc_titulo'];
    	$rentacarro->rc_descripcioncorta = $input['rc_descripcioncorta'];
    	$rentacarro->rc_descripcionlarga = $input['rc_descripcionlarga'];
    	$rentacarro->rc_img1 = $input['ho_img1'];
    	$rentacarro->rc_img2 = $input['ho_img2'];
    	$rentacarro->rc_img3 = $input['ho_img3'];
    	$rentacarro->de_registerUpdate = $input['de_registerUpdate'];
    	$rentacarro->de_userUpdate = $input['de_userUpdate'];

    	$rentacarro->save();

    	return $this->sendResponse($rentacarro->toArray(), 'RentaCarro actualizado exitosamente.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$rentacarro = RentaCarro::find($input['id']);
    	$rentacarro->delete();
    	return $this->sendResponse($rentacarro->toArray(), 'RentaCarro eliminada exitosamente.');
    }
}



