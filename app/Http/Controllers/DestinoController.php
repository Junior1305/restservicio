<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Destino;
use Validator; 

class DestinoController extends BaseController
{

	public function __construct(){
		$this->middleware('cors');
	}

	public function index()
	{
		$destinos = Destino::all();
		return $this->sendResponse($destinos->toArray(), 'Destinos enviados exitosamente.');
	}


	public function list()
	{

		$destinos = Destino::where('de_estado','1')->get();
		return $this->sendResponse($destinos->toArray(), 'Destinos enviados exitosamente.');
	}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$input['de_registerDate'] = date('Y-m-d H:i:s');
    	$input['de_registerUpdate'] = date('Y-m-d H:i:s');
    	
    	if($input['de_titprincipal'] == '' || $input['ciudad_id'] == null ){
    		return $this->sendError('','Destino no registrado.');       
    	}

    	$destino = Destino::create($input);


    	return $this->sendResponse($destino->toArray(), 'Destino creada exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);

    	$destino = Destino::find($input['id']);

    	if (is_null($destino)) {
    		return $this->sendError(null,'Destino no encontrado.');
    	}


    	return $this->sendResponse($destino->toArray(), 'Destino enviado exitosamente.');
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$destino = Destino::find($input['id']);
    	$input['de_registerUpdate'] = date('Y-m-d H:i:s');
    	if (is_null($destino)) {
    		return $this->sendError($destino->toArray(),'Destino no encontrado.');
    	}

    	$destino->ciudad_id = $input['ciudad_id'];
    	$destino->de_titpequeno = $input['de_titpequeno'];
    	$destino->de_titprincipal = $input['de_titprincipal'];
    	$destino->de_descripcioncorta = $input['de_descripcioncorta'];
    	$destino->de_descripcionlarga = $input['de_descripcionlarga'];
    	$destino->de_img1 = $input['de_img1'];
    	$destino->de_img2 = $input['de_img2'];
    	$destino->de_img3 = $input['de_img3'];
    	$destino->de_registerUpdate = $input['de_registerUpdate'];
    	$destino->de_userUpdate = $input['de_userUpdate'];

    	$destino->save();


    	return $this->sendResponse($destino->toArray(), 'Destino actualizado exitosamente.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$destino = Destino::find($input['id']);
    	$destino->delete();
    	return $this->sendResponse($destino->toArray(), 'Destino eliminada exitosamente.');
    }
}



