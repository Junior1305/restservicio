<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Pais;
use Validator; 

class PaisController extends BaseController
{

	public function __construct(){
		$this->middleware('cors');
	}

	public function index()
	{
		$paises = Pais::all();
		return $this->sendResponse($paises->toArray(), 'Paises enviados exitosamente.');
	}


	public function list()
	{

		$paises = Pais::where('pa_estado','1')->get();
		return $this->sendResponse($paises->toArray(), 'Paises enviados exitosamente.');
	}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$input['pa_registerDate'] = date('Y-m-d H:i:s');
    	$input['pa_registerUpdate'] = date('Y-m-d H:i:s');
    	
    	if($input['pa_nombre'] == '' ){
    		return $this->sendError('','Pais no registrado.');       
    	}

    	$pais = Pais::create($input);


    	return $this->sendResponse($pais->toArray(), 'Pais creado exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);

    	$pais = Pais::find($input['id']);

    	if (is_null($pais)) {
    		return $this->sendError(null,'Pais no encontrado.');
    	}


    	return $this->sendResponse($pais->toArray(), 'Pais enviado exitosamente.');
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$pais = Pais::find($input['id']);
    	$input['pa_registerUpdate'] = date('Y-m-d H:i:s');
    	if (is_null($pais)) {
    		return $this->sendError($pais->toArray(),'Pais no encontrado.');
    	}

    	$pais->pa_nombre = $input['pa_nombre'];
    	$pais->pa_registerUpdate = $input['pa_registerUpdate'];
    	$pais->pa_userUpdate = $input['pa_userUpdate'];

    	$pais->save();


    	return $this->sendResponse($pais->toArray(), 'Pais actualizado exitosamente.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$pais = Pais::find($input['id']);
    	$pais->delete();
    	return $this->sendResponse($pais->toArray(), 'Pais eliminado exitosamente.');
    }
}
