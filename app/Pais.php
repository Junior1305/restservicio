<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
	public $table = "pais";
	public $timestamps = false;
	protected $fillable = [
		'pa_nombre','pa_estado','pa_registerDate','pa_registerUpdate','pa_userUpdate'
	];
}
