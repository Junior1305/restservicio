<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarifario extends Model
{
	public $table = "tarifario";
	public $timestamps = false;
	protected $fillable = [
		'ta_tipoServicio','proveedor_id','ta_precio','ta_estado','ta_registerDate','ta_registerUpdate','ta_userUpdate'
	];
}
