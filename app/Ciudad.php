<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
	public $table = "ciudad";
	public $timestamps = false;
	protected $fillable = [
		'pais_id','ci_nombre','ci_estado','ci_registerDate','ci_registerUpdate','ci_userUpdate'
	];
}
